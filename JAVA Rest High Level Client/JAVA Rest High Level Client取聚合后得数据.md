一、聚合查询：对于下面这个简单的聚合，目的是对于文档全文匹配，聚`sysId`字段。把满足匹配的文档放入自定义名称的`agg_sysId`桶中

```
{
    "size" : 0,
    "query" : {
        "match_all" : {}
    },
    "aggs" : {
        "agg_sysId" : {
            "terms" : {
              "field" : "logId.sysId"
            }
        }
    }
}
```

聚合结果：

```json
{
    "took": 6,
    "timed_out": false,
    "_shards": {
        "total": 5,
        "successful": 5,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 216364,
        "max_score": 0.0,
        "hits": []
    },
    "aggregations": {
        "agg_sysId": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
                {
                    "key": "query",
                    "doc_count": 215212
                },
                {
                    "key": "log",
                    "doc_count": 903
                },
                {
                    "key": "tms",
                    "doc_count": 233
                },
                {
                    "key": "acs",
                    "doc_count": 16
                }
            ]
        }
    }
}
```

二， 对照着这个DSL写我们的java rest client api的调用方法，供业务层调用

```java
	/**
     * ES中查询所有color
     *
     * @param indices
     * @return
     */
    public SearchResponse getAllColor(String... indices) {
        SearchRequest searchRequest = new SearchRequest(indices);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        TermsAggregationBuilder builder = AggregationBuilders.terms("colors").field("color");
        searchSourceBuilder.aggregation(builder);
        searchRequest.source(searchSourceBuilder);

        try {
            return client.getHighLevelClient().search(searchRequest);
        } catch (IOException e) {
            throw new BizException("聚合失败：{}", e.getCause());
        }
    }
```

三， 在我们的业务层调用上文中的getAllColor方法获取response，对response解析，获取聚合的结果

```java
	/* 颜色信息
     *
     * @return
     */
    public List<String> getAllColorInfo() {
        SearchResponse response = highRestHelper.getAllColor(getIndices());
        Aggregations aggregations = response.getAggregations();
        ParsedStringTerms colorTerms = aggregations.get("app_group");
        List<String> colors = new ArrayList<>();
        List<? extends Terms.Bucket> buckets = colorTerms.getBuckets();
        for (Terms.Bucket bucket : buckets) {
            String appName = bucket.getKey().toString();
            colors.add(appName);
        }   
        return colors;
    }
```

至此即可获取所有的聚合结果，只要能够参照DSL，转换成相应的api调用，那么解析过程完全可以复用。

