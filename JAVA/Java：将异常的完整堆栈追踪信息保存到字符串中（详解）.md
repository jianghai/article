Java：将异常的完整堆栈追踪信息保存到字符串中（详解）

```java
import java.io.*;

public class JunitTest {
    public static void main(String args[])  {
        try {
            File file = new File("D:/abc*");//制造异常
            FileOutputStream fos=new FileOutputStream(file);
            fos.write("some conent.".getBytes());
        } catch (FileNotFoundException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw =new PrintWriter(sw,true);
            e.printStackTrace(pw);
            e.printStackTrace();//控制台异常
            String stackTraceString = sw.getBuffer().toString();
            System.out.println("FileNotFoundException stackTraceString:"+stackTraceString);//我们获取到的异常，方便我们保存到数据库中
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw =new PrintWriter(sw,true);
            e.printStackTrace(pw);
            e.printStackTrace();
            String stackTraceString = sw.getBuffer().toString();
            System.out.println("IOException stackTraceString:"+stackTraceString);
        }

    }
}
```

console print：

```java
e.printStackTrace()：
java.io.FileNotFoundException: D:\abc* (文件名、目录名或卷标语法不正确。)
	at java.io.FileOutputStream.open0(Native Method)
	at java.io.FileOutputStream.open(FileOutputStream.java:270)
	at java.io.FileOutputStream.<init>(FileOutputStream.java:213)
	at java.io.FileOutputStream.<init>(FileOutputStream.java:162)
	at autoTest.JunitTest.main(JunitTest.java:9)
System.out.println：
FileNotFoundException stackTraceString:java.io.FileNotFoundException: D:\abc* (文件名、目录名或卷标语法不正确。)
	at java.io.FileOutputStream.open0(Native Method)
	at java.io.FileOutputStream.open(FileOutputStream.java:270)
	at java.io.FileOutputStream.<init>(FileOutputStream.java:213)
	at java.io.FileOutputStream.<init>(FileOutputStream.java:162)
	at autoTest.JunitTest.main(JunitTest.java:9)
```

