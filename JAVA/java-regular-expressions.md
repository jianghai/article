(?:pattern)、(?=pattern)、(?<=pattern)、(?!pattern)和(?<!pattern)

```java
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
    public static void main( String args[] ){ 
      
	    String password="Windows";
        String regex="^(?=W)\\w+(?<=s)$";
        Pattern p=Pattern.compile(regex);
        Matcher m=p.matcher(password);
        boolean isMatch=m.matches();
        System.out.println(isMatch);
		 if (m.find( )) {
			 System.out.println("Found value: " + m.group(0) );     
			 System.out.println("Found value: " + m.group(1) );
		  } else {
			 System.out.println("NO MATCH");
		  }
		
		String password2="Windows";
        String regex2="^\\w{1}(?=i)\\w+$";
        Pattern p2=Pattern.compile(regex2);
        Matcher m2=p2.matcher(password2);
        boolean isMatch2=m2.matches();
        System.out.println(isMatch2);
		if (m2.find( )) {
			 System.out.println("Found value: " + m2.group(0) );         
		  } else {
			 System.out.println("NO MATCH");
		  }
		
		String password3="Windows";
        String regex3="^(?!a)\\w+(?<!a)$";
        Pattern p3=Pattern.compile(regex3);
        Matcher m3=p.matcher(password3);
        boolean isMatch3=m.matches();
        System.out.println(isMatch3);
		if (m3.find( )) {
			 System.out.println("Found value: " + m3.group(0) );         
		  } else {
			 System.out.println("NO MATCH");
		  }
		
		String password4="Windows";
        String regex4="^\\w{1}(?!a)\\w+$";
        Pattern p4=Pattern.compile(regex4);
        Matcher m4=p4.matcher(password4);
        boolean isMatch4=m4.matches();
        System.out.println(isMatch4);
		if (m4.find( )) {
			 System.out.println("Found value: " + m4.group(0) );         
		  } else {
			 System.out.println("NO MATCH");
		  }
		
		String password5="Windows";
        String regex5="^\\w{1}(?:i)\\w+$";
        Pattern p5=Pattern.compile(regex5);
        Matcher m5=p5.matcher(password4);
        boolean isMatch5=m5.matches();
        System.out.println(isMatch5);
		if (m5.find( )) {
			 System.out.println("Found value: " + m5.group(0) );         
		  } else {
			 System.out.println("NO MATCH");
		  }
		
   }
}
```



result



```java
true
NO MATCH
true
NO MATCH
true
Found value: Windows
true
NO MATCH
true
NO MATCH
```



 

## start 和 end 方法

```java
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
    private static final String REGEX = "[4]*cat[123]*";
    private static final String INPUT =
                                    "cat1 cat2 cat3 cattie 4cat";
 
    public static void main( String args[] ){
       Pattern p = Pattern.compile(REGEX);
       Matcher m = p.matcher(INPUT); // 获取 matcher 对象
       int count = 0;
 
       while(m.find()) {
         count++;
         System.out.println("Match number "+count);
         System.out.println("start(): "+m.start());
         System.out.println("end(): "+m.end());
		   System.out.println("group(): "+m.group());
      }
   }
}
```

包含：

```java
import java.util.regex.*;
 
class RegexExample1{
   public static void main(String args[]){
      String content = "I am noob " +
        "from runoob.com.";
 
      String pattern = ".*runoob.*";
 
      boolean isMatch = Pattern.matches(pattern, content);
      System.out.println("字符串中是否包含了 'runoob' 子字符串? " + isMatch);
   }
}
```





## 捕获组

```
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
    public static void main( String args[] ){
 
      // 按指定模式在字符串查找
      String line = "This order was placed for QT3000! OK?";
      String pattern = "(\\D*)(\\d+)(.*)";
 
      // 创建 Pattern 对象
      Pattern r = Pattern.compile(pattern);
 
      // 现在创建 matcher 对象
      Matcher m = r.matcher(line);
      if (m.find( )) {
         System.out.println("Found value: " + m.group(0) );
         System.out.println("Found value: " + m.group(1) );
         System.out.println("Found value: " + m.group(2) );
         System.out.println("Found value: " + m.group(3) ); 
      } else {
         System.out.println("NO MATCH");
      }
   }
}
```

result

```java
Found value: This order was placed for QT3000! OK?
Found value: This order was placed for QT
Found value: 3000
Found value: ! OK?
```

## matches 和 lookingAt 方法

```java
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
    private static final String REGEX = "foo";
    private static final String INPUT = "fooooooooooooooooo";
    private static final String INPUT2 = "ooooofoooooooooooo";
    private static Pattern pattern;
    private static Matcher matcher;
    private static Matcher matcher2;
 
    public static void main( String args[] ){
       pattern = Pattern.compile(REGEX);
       matcher = pattern.matcher(INPUT);
       matcher2 = pattern.matcher(INPUT2);
 
       System.out.println("Current REGEX is: "+REGEX);
       System.out.println("Current INPUT is: "+INPUT);
       System.out.println("Current INPUT2 is: "+INPUT2);
 
 
       System.out.println("lookingAt(): "+matcher.lookingAt());
       System.out.println("matches(): "+matcher.matches());
       System.out.println("lookingAt(): "+matcher2.lookingAt());
   }
}
```

result

```java
Current REGEX is: foo
Current INPUT is: fooooooooooooooooo
Current INPUT2 is: ooooofoooooooooooo
lookingAt(): true
matches(): false
lookingAt(): false
```

## replaceFirst 和 replaceAll 方法

```
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
    private static String REGEX = "dog";
    private static String INPUT = "The dog says meow. " +
                                    "All dogs say meow.";
    private static String REPLACE = "cat";
 
    public static void main(String[] args) {
       Pattern p = Pattern.compile(REGEX);
       // get a matcher object
       Matcher m = p.matcher(INPUT);		
       System.out.println(m.replaceFirst(REPLACE));
	   System.out.println(m.replaceAll(REPLACE));
	   System.out.println(INPUT);
   }
}
```

result

```java
The cat says meow. All dogs say meow.
The cat says meow. All cats say meow.
The dog says meow. All dogs say meow.
```



**appendReplacement 和 appendTail 方法**



```java
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class RegexMatches
{
   private static String REGEX = "a*b";
   private static String INPUT = "aabfooaabfooabfoobkkk";
   private static String REPLACE = "-";
   public static void main(String[] args) {
      Pattern p = Pattern.compile(REGEX);
      // 获取 matcher 对象
      Matcher m = p.matcher(INPUT);
      StringBuffer sb = new StringBuffer();
      while(m.find()){
         m.appendReplacement(sb,REPLACE);
		  System.out.println("sb.toString():"+sb.toString()+",INPUT:"+INPUT);
      }
      m.appendTail(sb);
      System.out.println(sb.toString());
   }
}
```

result

```java
sb.toString():-,INPUT:aabfooaabfooabfoobkkk
sb.toString():-foo-,INPUT:aabfooaabfooabfoobkkk
sb.toString():-foo-foo-,INPUT:aabfooaabfooabfoobkkk
sb.toString():-foo-foo-foo-,INPUT:aabfooaabfooabfoobkkk
-foo-foo-foo-kkk
```





[相关链接](https://www.runoob.com/java/java-regular-expressions.html )