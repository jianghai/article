Java调用javascript函数



```java
import org.springframework.scripting.support.StandardScriptEvaluator;
import org.springframework.scripting.support.StaticScriptSource;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

public class JunitTest {


    //script type
    static String scriptType = "javascript";

    //script source
    static String scriptSource = "function myFunction(p1, p2) {\n" +
            "    return p1 * p2;\n" +
            "}";

    public static Object execSpringJS(){
        //argument
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("argument1", 2);
        arguments.put("argument2", 9);
        
        StandardScriptEvaluator evaluator = new StandardScriptEvaluator();

        //set script type
        evaluator.setLanguage(scriptType);

        //execute the script
        Object resultEngine =   evaluator.evaluate(new StaticScriptSource(scriptSource), arguments);
        if(resultEngine instanceof Invocable ){
            try {
                Invocable invoke = (Invocable) resultEngine;
                return invoke.invokeFunction("myFunction");
            } catch (ScriptException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        return null;
    }



    public static Double execJS() throws ScriptException, NoSuchMethodException {
        ScriptEngineManager manager = new ScriptEngineManager();
        //ScriptEngineManager可以理解为一个工厂，根据你输入的语言名字，返回调用的函数库。
        ScriptEngine jsEngine = manager.getEngineByName("javascript");
        String jsFileName = "log-ui/src/test/java/comm.js";
        FileReader reader = null;
        try {
            reader = new FileReader(jsFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //通过eval函数读取Engine实例
        try {
            jsEngine.eval(reader);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        Double result = null;
        if (jsEngine instanceof Invocable) {
            Invocable invoke = (Invocable) jsEngine;
            //SHA256是js中的function名字。
            result = (Double) invoke.invokeFunction("myFunction", 2,4);
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static void main(String[] args) throws ScriptException, NoSuchMethodException {
//        System.out.println(execJS());
//        System.out.println(execSpringJS());
    }
}

```

comm.js

```javascript
function myFunction(p1, p2) {
    return p1 * p2;
}
```

